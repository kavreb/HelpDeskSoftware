﻿Help Desk Submissions form

Built with C# and ASP.NET MVC in Visual Studio. 

Website allows the user to add assignments and deadlines into the assignments list.
Description and deadline are mandatory from the user. 
ID number and creation date are created automatically by the program.

List can be seen under the submission form. 
Fulfilled assignments can be removed with the click of the appropriate button.

Project was tested with NUnit and NCrunch.