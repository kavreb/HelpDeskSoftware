﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelpDeskSoftware.Models;
using HelpDeskSoftware.Services;

namespace HelpDeskSoftware.Controllers
{
    public class TicketsController : Controller
    {
        public ActionResult Index(int? ticketId)
        {
            TicketRepository.CreateTickets();

            if (ticketId != null)
            {
                var doneTicket = Tickets.Find(x => x.TicketId == ticketId);
                Tickets.Remove(doneTicket);
            }

            return View(Tickets.OrderByDescending(x => x.Deadline).ToList());
        }

        public static readonly List<Ticket> Tickets = new List<Ticket>();

        [HttpPost]
        public ActionResult Add(string ticket, string deadline)
        {
            DateTime entryTime = DateTime.Now;
            int ticketId;

            if (!Tickets.Any())
            {
                ticketId = 1;
            }
            else if (Tickets.Find(x => x.Description == ticket) != null)
            {
                TempData["DuplicateItem"] = "Inserted ticket is already on the list.";
                return RedirectToAction("Index");
            }
            else
            {
                ticketId = (Tickets.Last()).TicketId + 1;
            }

            if (!DateTime.TryParse(deadline, out DateTime validDeadline))
            {
                TempData["WrongInputError"] = "Please insert correct date/time.";
                return RedirectToAction("Index");
            }
            else if (validDeadline.Date < DateTime.Now.Date)
            {
                TempData["WrongDateError"] = "Please insert correct date";
                return RedirectToAction("Index");
            }

            var newTicket = new Ticket(ticketId, ticket, validDeadline.ToString(), entryTime);
            Tickets.Add(newTicket);

            return RedirectToAction("Index");
        }
    }
}