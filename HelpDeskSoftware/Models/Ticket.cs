﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HelpDeskSoftware.Controllers;

namespace HelpDeskSoftware.Models
{
    public class Ticket
    {
        public int TicketId { get; set; }

        public string Description { get; set; }

        public DateTime TimeOfEntry { get; set; }

        public DateTime Deadline { get; set; }

        public Ticket(int id, string ticket, string deadline, DateTime entrytime)
        {
            TicketId = id;
            Description = ticket;
            TimeOfEntry = entrytime;

            DateTime correctDeadline = DateTime.Parse(deadline);
                        
            if (correctDeadline.Date == TimeOfEntry.Date
                && correctDeadline.TimeOfDay <= TimeOfEntry.TimeOfDay)
            {
                Deadline = correctDeadline.AddDays(1);
            }
            else
            {
                Deadline = correctDeadline;
            }            
        }
    }
}