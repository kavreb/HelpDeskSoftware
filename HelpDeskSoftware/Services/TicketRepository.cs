﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HelpDeskSoftware.Models;
using HelpDeskSoftware.Controllers;

namespace HelpDeskSoftware.Services
{
    public class TicketRepository
    {

        public static List<Ticket> Objects = new List<Ticket>
            {
                new Ticket(1, "Fix Mouse", "2019.03.14 12:22", DateTime.Now),
                new Ticket(2, "Repair computer", "2018.04.15 15:55", DateTime.Now),
                new Ticket(3, "Post for testing", "2018.03.20 14:00", DateTime.Now),
                new Ticket(4, "New monitor to QA", "2018.05.01 15:00", DateTime.Now),
                new Ticket(5, "Return my book!", "2017.05.16 00:00", DateTime.Now),
                new Ticket(6, "Invoice for JK AS", "2018.03.30 15:00", DateTime.Now)
            };

        public static void CreateTickets()
        {
            foreach (var item in Objects)
            {
                if (!TicketsController.Tickets.Contains(item))
                {
                    TicketsController.Tickets.Add(item);
                }
            }
        }
    }
}