﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using HelpDeskSoftware;
using HelpDeskSoftware.Controllers;
using HelpDeskSoftware.Models;
using NUnit.Framework;
using HelpDeskSoftware.Services;

namespace HelpDeskSoftware.Tests.Controllers
{

    public class HomepageTests
    {
        [Test]
        public void AddControllerRedirects_ToIndex()
        {
            var controller = new TicketsController();

            var result = controller.Add("Repair mouse", "15:00") as RedirectToRouteResult;

            Assert.That(result, Is.Not.Null);
            Assert.That("Index", Is.EqualTo(result.RouteValues["action"]));
        }

        [Test]
        public void DeleteTicket_RemovesTicketFromList()
        {
            int ticketId = 1;
            var controller = new TicketsController();
            Ticket task = new Ticket(ticketId, "Repair", "15:00", DateTime.Now);
            TicketsController.Tickets.Clear();
            TicketsController.Tickets.Add(task);

            var result = controller.Index(ticketId);

            CollectionAssert.DoesNotContain(TicketsController.Tickets, task);
            CollectionAssert.AllItemsAreNotNull(TicketsController.Tickets);
        }

        [Test]
        public void PremadeTicketsList_WontAddDuplicates()
        {
            TicketsController.Tickets.Clear();

            TicketRepository.CreateTickets();
            TicketRepository.CreateTickets();
            
            CollectionAssert.AllItemsAreUnique(TicketsController.Tickets.Select(x => x.Description));
        }

        [Test]
        [TestCase("2018.02.15")]
        [TestCase("2018.03.14 15:00")]
        [TestCase("2018.03.14 15:31")]
        [TestCase("2018.03.14 16:31")]
        [TestCase("2018.03.14 17:00")]
        [TestCase("2018.04.15 12:00")]
        public void DeadlineCloseOrPassed_IndexPageTicketColourChanges(string input)
        {
            var today = new DateTime(2018, 03, 14, 15, 30, 00);
            DateTime.TryParse(input, out DateTime deadline);
            var timeDifference = (today > deadline ? (today - deadline) : (deadline - today)).TotalMinutes;

            if ((deadline.Date == today.Date
                    && deadline.Hour < today.Hour)
                || deadline.Date < today.Date
                || (deadline.Date == today.Date
                    && timeDifference <= 60))
            {
                Assert.That(deadline, Is.LessThanOrEqualTo(today.AddMinutes(60)));
            }
            else
            {
                Assert.That(deadline, Is.Not.LessThanOrEqualTo(today));
            }
        }
    }


    public class CreateTicketTests
    {
        [Test]
        public void AddTicket_AddsTicketToList()
        {
            var checkedList = TicketsController.Tickets;

            checkedList.Clear();
            var task = new Ticket(1, "Repair mouse", "15:00", DateTime.Now);
            checkedList.Add(task);

            CollectionAssert.IsNotEmpty(checkedList);
            CollectionAssert.Contains(checkedList, task);
        }


        [Test]
        public void NewTicketValues_GetsPassedToList()
        {
            string description = "Repair mouse";
            string deadline = "2018.06.15 15:00";
            var controller = new TicketsController();

            TicketsController.Tickets.Clear();
            var task = controller.Add(description, deadline);
            
            Assert.That(TicketsController.Tickets.First().Description, Is.EqualTo(description));
            Assert.That(TicketsController.Tickets.First().Deadline, Is.EqualTo(DateTime.Parse(deadline)));
        }

        [Test]
        [TestCase("2018.05.35")]
        [TestCase("15:89")]
        [TestCase("26:15")]
        [TestCase("2020.02.29 15:23")]
        [TestCase("15:55")]
        public void NewDeadline_AdheresToMaxNumbers(string dlString)
        {
            var controller = new TicketsController();
            TicketsController.Tickets.Clear();

            var task1 = controller.Add("Repair", dlString);

            if (!DateTime.TryParse(dlString, out DateTime correctDeadline))
            {
                Assert.That(TicketsController.Tickets, Is.Empty);
            }
            else
            {
                var dlValue = TicketsController.Tickets.FirstOrDefault().Deadline;

                Assert.That(dlValue.Minute, Is.Not.GreaterThan(60));
                Assert.That(dlValue.Hour, Is.Not.GreaterThan(24));
                Assert.That(dlValue.Day, Is.Not.GreaterThan
                                           (CultureInfo.CurrentCulture.Calendar.GetDaysInMonth
                                           (dlValue.Year, dlValue.Month)))
                                           ;
            }
            
        }

        [Test]
        [TestCase("2018.03.14 14:00")]
        [TestCase("2018.03.14 15:31")]
        [TestCase("2018.04.24 16:00")]
        [TestCase("2018.04.24 12:00")]
        public void NewDeadlineEarlierSameDay_SetDateNextDay(string input)
        {
            var today = new DateTime(2018, 03, 14, 15, 30, 00);
            var dtInput = DateTime.Parse(input);

            var task = new Ticket(1, "Repair", input, today);

            if (dtInput.Date == today.Date
                && dtInput.TimeOfDay <= today.TimeOfDay)
            {
                Assert.That(task.Deadline, Is.GreaterThan(today));
            }
            else 
            {
                Assert.That(task.Deadline, Is.Not.LessThan(today));
                Assert.That(task.Deadline, Is.EqualTo(dtInput));
            }
        }

        [Test]
        [TestCase("Not datetime")]
        [TestCase("2017.12.12 14:55")]
        [TestCase("2020.06.15 14:55")]
        public void NewDeadline_OnlyValidIsAccepted(string input)
        {
            var controller = new TicketsController();
            TicketsController.Tickets.Clear();

            var task = controller.Add("Repair", input);

            if (!DateTime.TryParse(input, out DateTime validDeadline))
            {
                Assert.That(TicketsController.Tickets.Select(x => x.TimeOfEntry), Is.Empty);
            }
            else if (validDeadline.Date < DateTime.Now.Date)
            {
                Assert.That(TicketsController.Tickets.Select(x => x.TimeOfEntry), Is.Empty);
            }
            else
            {
                Assert.That(TicketsController.Tickets.Select(x => x.TimeOfEntry), Is.Not.Empty);
            }
        }
        
        [Test]
        public void SetTimeOfEntry_AutomaticallyOnCreation()
        {
            var controller = new TicketsController();
            TicketsController.Tickets.Clear();

            var task = controller.Add("Repair", "15:00");

            Assert.That(TicketsController.Tickets[0].TimeOfEntry.Date, Is.EqualTo(DateTime.Now.Date));
        }

        [Test]
        public void SetUserId_AutomaticallyOnCreation()
        {
            var controller = new TicketsController();
            TicketsController.Tickets.Clear();

            var input1 = controller.Add("Repair", "15:00");
            var input2 = controller.Add("Remodel", "15:00");

            CollectionAssert.AllItemsAreUnique(TicketsController.Tickets.Select(x => x.TicketId));
            Assert.That(TicketsController.Tickets[1].Description, Is.EqualTo("Remodel"));
        }

        [Test]
        public void DuplicateTicket_GetsRejected()
        {
            var input = "Repair computer";
            var controller = new TicketsController();
            TicketsController.Tickets.Clear();

            var task1 = controller.Add(input, "15:00");
            var task2 = controller.Add(input, "16:00");

            CollectionAssert.AllItemsAreUnique(TicketsController.Tickets.Select(x => x.Description));
        }
    }
}
